# MyCata

Projet de cata des séries, lectures, films vus...

## Le projet

Projet en Symfony 5 et certainement ajout de JS vanilla pour certaines features. Pour le design, on va se contenter d'un bootstrap 4.5.2 en attendant la version 5.

## Page d'accueil

- Création d'un HomeController pour la page d'accueil.
- Ajout navbar bootstrap dans base.html.twig + exemple carousel pour la page d'accueil (index.html.twig).
- Création fichier css pour l'index => index.css

## Environnement

- Création du fichier env.local.

## Anime

- Création entité Anime avec les champs : titre(title), résumé(summary), dernier épisode vu(lastEpisodeSeen), image(image), (liste à compléter plus tard).
- Création AnimeController avec CRUD.
