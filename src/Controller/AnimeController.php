<?php

namespace App\Controller;

use App\Entity\Anime;
use App\Form\AnimeType;
use App\Repository\AnimeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/anime")
 */
class AnimeController extends AbstractController
{
    /**
     * @Route("/", name="anime_index", methods={"GET"})
     */
    public function index(AnimeRepository $animeRepository): Response
    {
        return $this->render('anime/index.html.twig', [
            'animes' => $animeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="anime_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $anime = new Anime();
        $form = $this->createForm(AnimeType::class, $anime);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($anime);
            $entityManager->flush();

            return $this->redirectToRoute('anime_index');
        }

        return $this->render('anime/new.html.twig', [
            'anime' => $anime,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="anime_show", methods={"GET"})
     */
    public function show(Anime $anime): Response
    {
        return $this->render('anime/show.html.twig', [
            'anime' => $anime,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="anime_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Anime $anime): Response
    {
        $form = $this->createForm(AnimeType::class, $anime);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('anime_index');
        }

        return $this->render('anime/edit.html.twig', [
            'anime' => $anime,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="anime_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Anime $anime): Response
    {
        if ($this->isCsrfTokenValid('delete'.$anime->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($anime);
            $entityManager->flush();
        }

        return $this->redirectToRoute('anime_index');
    }
}
